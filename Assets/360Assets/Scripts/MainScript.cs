﻿using UnityEngine;
public class MainScript : MonoBehaviour
{
    private RoomViewImpl view;
    public bool isOverPanel;
    public ArrowMono arrow;

    public void Start()
    {
        Debug.Log(System.Environment.Version);
        isOverPanel = false;
        view = gameObject.AddComponent<RoomViewImpl>() as RoomViewImpl;
        Debug.Log(view);
    }

    public void JavaDebug(string text)
    {
        Debug.Log(text);
    }

    public void makeQuit()
    {
        PlayerPrefs.SetInt("quit", 1);
        PlayerPrefs.Save();
    }

    public void setFalseOverPanel()
    {
        isOverPanel = false;
    }

    public void setTrueOverPanel()
    {
        isOverPanel = true;
    }
}