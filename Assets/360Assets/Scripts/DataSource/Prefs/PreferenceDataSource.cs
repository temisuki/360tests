using UnityEngine;

public class PreferenceDataSource : MonoBehaviour, IPreferenceDataSource
{
    public Tour getLastTour()
    {
        return JsonUtility.FromJson<Tour>(PlayerPrefs.GetString("LastTour"));
    }

    public string getTourId()
    {
#if UNITY_WEBGL
        string tourId = "8";
        int pm = Application.absoluteURL.IndexOf("?");
        if (pm != -1)
        {
            tourId = (Application.absoluteURL.Split("?"[0])[1]).Substring(3);
        }
        return tourId;
#endif

#if UNITY_ANDROID || UNITY_IOS
        int temp = PlayerPrefs.GetInt("Tour");
        if (temp == null || temp == 0) return "8";
        else return temp.ToString();
#endif
    }

    public void saveLastTour(Tour tour)
    {
        PlayerPrefs.SetString("LastTour", JsonUtility.ToJson(tour));
        PlayerPrefs.Save();
    }
}