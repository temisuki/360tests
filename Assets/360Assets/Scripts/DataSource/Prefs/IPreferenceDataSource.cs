﻿public interface IPreferenceDataSource
{
    Tour getLastTour();

    void saveLastTour(Tour tour);
    string getTourId();
}