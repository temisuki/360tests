﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ApiDataSource : MonoBehaviour, IApiDataSource
{
    private Action<Tour> tour;

    private int idTour;
    private GameObject loading;

    public void deleteArrow(int idArrow)
    {
        StartCoroutine(Delete(idArrow));
    }

    public void getTour(string tourId, System.Action<Tour> onDataLoad)
    {
        if (loading == null) loading = GameObject.FindGameObjectWithTag("Loading");
        loading.SetActive(false);
        StartCoroutine(Get(tourId, tour =>
        {
            onAction(tour, onDataLoad);
        }));
    }

    public void postArrow(Arrow arrow)
    {
        throw new NotImplementedException();
    }

    public void onAction(Tour tour, Action<Tour> action)
    {
        action(tour);
    }

    public void putArrow(Arrow arrow)
    {
        throw new NotImplementedException();
    }

    #region Courutines

    private IEnumerator Get(string dataId, System.Action<Tour> onDataLoad)
    {
        if (dataId == "") dataId = "3";
        string url = "http://360.actumlab.com/web/api/tours?tourid=" + dataId;
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            if (www.isDone)
            {
                onDataLoad(createObjects(www.downloadHandler.text));
                www.Dispose();
                www = null;
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
            }
        }
    }

    private IEnumerator GetTextures(Room room, System.Action<int> numberOfTextures)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(room.image);
        yield return www.Send();
        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            numberOfTextures(1);
            Resources.UnloadUnusedAssets();
        }
    }

    #endregion Courutines

    public Tour createObjects(string json)
    {
        json = json.Substring(1, json.Length - 2);
        json = json.Replace("default", "isFirst");
        return JsonUtility.FromJson<Tour>(json);
    }

    public void getTexture(Room room, System.Action<CallbackPattern> onDataLoad)
    {
        StartCoroutine(GetTexture(room, marker =>
        {
            onDataLoad(marker);
        }));
    }

    private IEnumerator GetTexture(Room room, System.Action<CallbackPattern> c)
    {
        loading.SetActive(true);
        loading.GetComponentInChildren<Text>().text = "0 %";
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(room.image);
        UnityEngine.AsyncOperation sendData = www.Send();

        while (!sendData.isDone)
        {
            loading.GetComponentInChildren<Image>().fillAmount = sendData.progress;
            if(sendData.progress == 1)
            {
                loading.GetComponentInChildren<Text>().text = "Wait";
            }
            else
            {
                loading.GetComponentInChildren<Text>().text = (String.Format("{0:N0} %", sendData.progress * 100));
            }
            yield return new WaitForEndOfFrame();
        }

        if (!www.isDone)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(room.image.Length);
            room.PhotoName = room.image.Substring(36);
            Debug.Log(room.image);
            loading.GetComponentInChildren<Image>().fillAmount = 0;
            loading.SetActive(false);
            SaveTextureToFile(DownloadHandlerTexture.GetContent(www), room.PhotoName);
            DestroyImmediate(DownloadHandlerTexture.GetContent(www));
            www.Dispose();
            www = null;
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
            CallbackPattern callbackPattern = new CallbackPattern();
            callbackPattern.isDownloadDone = true;
            c(callbackPattern);
        }
    }

    private IEnumerator GetThumbnail(Room room, System.Action<CallbackPattern> c)
    {
        throw new NotImplementedException();
    }

    private IEnumerator Delete(int id)
    {
        string url = "http://360.actumlab.com/web/api/arrows/" + id;
        UnityWebRequest www = UnityWebRequest.Delete(url);
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.responseCode);
        }
    }

    private void SaveTextureToFile(Texture2D texture, string filename)
    {
        File.WriteAllBytes(Path.Combine(Application.persistentDataPath, filename), texture.EncodeToJPG());
        DestroyImmediate(texture);
    }

    public Texture2D LoadTexture(string filename)
    {
        Texture2D tempTexture = new Texture2D(1, 1);
        tempTexture.LoadImage(File.ReadAllBytes(Path.Combine(Application.persistentDataPath, filename)));
        return tempTexture;
    }

    public int readRefAndroid()
    {
        return 0;
    }

    public string readRef()
    {
        return PlayerPrefs.GetInt("Tour").ToString();
    }

    public int readRefIos()
    {
        return PlayerPrefs.GetInt("Tour");
    }

    public string readRefWebGl()
    {
        throw new NotImplementedException();
    }

    public void postArrow(Arrow arrow, Action<CallbackPattern> onDataLoad)
    {
        throw new NotImplementedException();
    }

    public void getThumbnail(Room room, Action<CallbackPattern> callback)
    {
        throw new NotImplementedException();
    }
}