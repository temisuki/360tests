﻿using UnityEngine;

public class CallbackPattern
{
    public bool isDownloadDone;
    public string error;
    public Texture2D texture;
    public int id;
}