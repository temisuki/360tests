﻿using System;
using UnityEngine;

public interface IApiDataSource
{
    void putArrow(Arrow arrow);

    void onAction(Tour tour, Action<Tour> action);

    void postArrow(Arrow arrow, System.Action<CallbackPattern> onDataLoad);

    void deleteArrow(int idArrow);

    void getTour(string tourId, System.Action<Tour> onDataLoad);

    void getTexture(Room room, System.Action<CallbackPattern> callback);

    void getThumbnail(Room room, System.Action<CallbackPattern> callback);

    Texture2D LoadTexture(string filename);

    string readRef();

    Tour createObjects(string json);
}