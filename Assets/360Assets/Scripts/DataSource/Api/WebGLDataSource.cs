﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class WebGLDataSource : MonoBehaviour, IApiDataSource
{
    private GameObject loading;
    private GameObject load;
    private static string tourUrl = "http://360.actumlab.com/web/api/tours?tourid=";

    public Tour createObjects(string json)
    {
        json = json.Substring(1, json.Length - 2).Replace("default", "isFirst");
        return JsonUtility.FromJson<Tour>(json);
    }

    public void deleteArrow(int idArrow)
    {
        StartCoroutine(Delete(idArrow));
    }

    //callback type
    public void getTexture(Room room, Action<CallbackPattern> onDataLoad)
    {
        StartCoroutine(GetText(room.image, marker =>
        {
            onDataLoad(marker);
        }));
    }

    public void getThumbnail(Room room, Action<CallbackPattern> onDataLoad)
    {
        StartCoroutine(GetThumb(room.thumbnail, marker =>
        {
            onDataLoad(marker);
        }));
    }

    public void getTour(string tourId, Action<Tour> onDataLoad)
    {
        if (loading == null) loading = GameObject.FindGameObjectWithTag("Loading");
        if (load == null) load = GameObject.FindGameObjectWithTag("Load");
        loading.SetActive(false);
        StartCoroutine(Get(tourId, tour =>
        {
            onAction(tour, onDataLoad);
        }));
    }

    public Texture2D LoadTexture(string filename)
    {
        throw new NotImplementedException();
    }

    public void onAction(Tour tour, Action<Tour> action)
    {
        action(tour);
    }

    public void postArrow(Arrow arrow, Action<CallbackPattern> onDataLoad)
    {
        Debug.Log("Post");
        StartCoroutine(Post(arrow, onDataLoadCoroutine =>
        {
            onDataLoad(onDataLoadCoroutine);
        }));
    }

    public void putArrow(Arrow arrow)
    {
        Debug.Log("Put");
        StartCoroutine(Put(JsonUtility.ToJson(arrow), arrow.idarrow));
    }

    //geturl params
    public string readRef()
    {
        string tourId = "";
        int pm = Application.absoluteURL.IndexOf("?");
        if (pm != -1)
        {
            tourId = (Application.absoluteURL.Split("?"[0])[1]).Substring(3);
        }
        return tourId;
    }

    private IEnumerator Delete(int id)
    {
        string url = "http://360.actumlab.com/web/api/arrows/" + id;
        UnityWebRequest www = UnityWebRequest.Delete(url);
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.responseCode);
        }
    }

    private IEnumerator Get(string dataId, System.Action<Tour> onDataLoad)
    {
        string url = tourUrl + dataId;
        Debug.Log(url);
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            if (www.isDone)
            {
                onDataLoad(createObjects(www.downloadHandler.text));
                www.Dispose();
                www = null;
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
            }
        }
    }

    //------------------------------------------------------------------------------------------
    private IEnumerator GetThumb(String link, Action<CallbackPattern> onDataLoad)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(link);
        yield return www.Send();

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.error);
            yield return null;
        }
        if (www.isDone && string.IsNullOrEmpty(www.error))
        {
            CallbackPattern callbackPattern = new CallbackPattern();
            callbackPattern.texture = DownloadHandlerTexture.GetContent(www);
            callbackPattern.isDownloadDone = true;
            www.Dispose();
            www = null;
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
            onDataLoad(callbackPattern);
        }
    }

    private IEnumerator GetTexture(String link, Action<CallbackPattern> onDataLoad)
    {
        loading.SetActive(true);
        loading.GetComponentInChildren<Text>().text = "0 %";
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(link);
        UnityEngine.AsyncOperation sendData = www.Send();

        while (!sendData.isDone)
        {
            loading.GetComponentInChildren<Image>().fillAmount = sendData.progress;
            if (sendData.progress == 1)
            {
                loading.GetComponentInChildren<Text>().text = "Wait";
            }
            else
            {
                loading.GetComponentInChildren<Text>().text = (String.Format("{0:N0} %", sendData.progress * 100));
            }
            yield return new WaitForEndOfFrame();
        }

        if (!www.isDone)
        {
            Debug.Log(www.error);
        }
        else
        {
            CallbackPattern callbackPattern = new CallbackPattern();
            callbackPattern.texture = DownloadHandlerTexture.GetContent(www);
            callbackPattern.isDownloadDone = true;
            www.Dispose();
            www = null;
            onDataLoad(callbackPattern);
        }
    }
    //private IEnumerator GetText(String link, Action<CallbackPattern> onDataLoad)
    //{
    //    var watch = System.Diagnostics.Stopwatch.StartNew();
    //    //loading.SetActive(true);
    //    Texture2D tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
    //    WWW www = new WWW(link);
    //    yield return www;
    //    if (www.isDone)
    //    {
    //        www.LoadImageIntoTexture(tex);
    //        CallbackPattern callbackPattern = new CallbackPattern();
    //        callbackPattern.texture = tex;
    //        callbackPattern.isDownloadDone = true;
    //        onDataLoad(callbackPattern);
    //        www.Dispose();
    //        www = null;
    //        watch.Stop();
    //        var elapsedMs = watch.ElapsedMilliseconds;
    //        Debug.Log(elapsedMs + " " + tex.width + " " + tex.height);
    //    }
    //}

    private IEnumerator GetText(String link, Action<CallbackPattern> onDataLoad)
    {
        var watch = System.Diagnostics.Stopwatch.StartNew();
        Texture2D tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(link);
        yield return www.Send();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            tex = DownloadHandlerTexture.GetContent(www);
            CallbackPattern callbackPattern = new CallbackPattern();
            callbackPattern.texture = tex;
            callbackPattern.isDownloadDone = true;
            onDataLoad(callbackPattern);
            www.Dispose();
            www = null;
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Debug.Log(elapsedMs + " " + tex.width + " " + tex.height);
        }
    }

    private IEnumerator Post(Arrow arrow, System.Action<CallbackPattern> onDataLoad)
    {
        string url = "http://360.actumlab.com/web/api/arrows";
        string json = JsonUtility.ToJson(arrow);
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.Send();

        if (request.isNetworkError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Debug.Log(request.responseCode);
            Debug.Log(request.downloadHandler.text);
            CallbackPattern callbackPattern = new CallbackPattern();
            callbackPattern.id = JsonUtility.FromJson<ArrowIdHandler>(request.downloadHandler.text).idarrow;
            onDataLoad(callbackPattern);
        }
    }

    private IEnumerator Put(string json, int id)
    {
        string url = "http://360.actumlab.com/web/api/arrows/" + id;
        Debug.Log(url);
        Debug.Log(json);
        byte[] postData = System.Text.Encoding.ASCII.GetBytes(json);

        UnityWebRequest www = UnityWebRequest.Put(url, postData);
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.responseCode);
        }
    }
}