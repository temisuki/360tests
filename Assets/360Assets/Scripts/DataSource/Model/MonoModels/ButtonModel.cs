﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonModel : MonoBehaviour
{
    public string thumbnailText;
    public int destinationRoom;
    public Texture2D texture;
    private Contract.RoomPresenter presenter;

    public void SetList(GameObject parent, Room room, CallbackPattern callbackPattern, Contract.RoomPresenter presenter)
    {
        destinationRoom = room.idroom;
        this.presenter = presenter;
        if (thumbnailText == "") thumbnailText = (presenter.findRoom(destinationRoom));
        this.gameObject.name = thumbnailText;
        this.gameObject.GetComponentInChildren<SetThumbText>().setTexture(callbackPattern.texture);
        this.gameObject.GetComponentInChildren<Text>().text = thumbnailText;
        this.gameObject.transform.SetParent(parent.transform);
    }

    public void MoveToRoom()
    {
        presenter.getRoom(destinationRoom);
    }

    public void SetDestination()
    {
        GameObject.FindGameObjectWithTag("Settings").GetComponent<MainScript>().arrow.setDestination(destinationRoom, thumbnailText);
        presenter.setPanelActive(false);
    }
}