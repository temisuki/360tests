﻿using UnityEngine;
using UnityEngine.UI;

public class ArrowMono : MonoBehaviour
{
    public int idarrow;
    public double x;
    public double y;
    public double z;
    public double xrot;
    public double yrot;
    public double zrot;
    public int destination;
    public int room_idroom;

    private Contract.RoomPresenter presenter;
    private string destinationName = "";

    public void SetObject(Arrow arrow, Contract.RoomPresenter presenter)
    {
        MakeObject(presenter, arrow);
        this.gameObject.transform.position = new Vector3((float)x, (float)y, (float)z);
        this.gameObject.transform.Rotate(new Vector3((float)xrot, (float)yrot, (float)zrot));
        
    }

    public void SetObject(Contract.RoomPresenter presenter, Arrow arrow)
    {
        MakeObject(presenter, arrow);
    }

    public void MakeObject(Contract.RoomPresenter presenter, Arrow arrow)
    {
        idarrow = arrow.idarrow;
        x = arrow.x;
        y = arrow.y;
        z = arrow.z;
        xrot = arrow.xrot;
        yrot = arrow.yrot;
        zrot = arrow.zrot;
        destination = arrow.destination;
        room_idroom = arrow.room_idroom;
        this.presenter = presenter;
        if (destination != 0)
        {
            if (destinationName == "") destinationName = (presenter.findRoom(destination));
        }
        this.gameObject.name = destinationName;
        this.gameObject.GetComponentInChildren<Text>().text = destinationName;
        this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag("CreatedArrows").transform);
    }
    public void setDestination(int destination, string destinationName)
    {
        this.gameObject.GetComponentInChildren<Text>().text = destinationName;
        this.destination = destination;
        presenter.addArrow(this);
    }

    public void Move()
    {
        presenter.getRoom(destination);
    }

    public void Delete()
    {
        presenter.deleteArrow(this);
    }

    public void Edit()
    {
        GameObject.FindGameObjectWithTag("Settings").GetComponent<MainScript>().arrow = this;
        presenter.setPanelActive(true);
    }

    public Arrow GetArrowFromMono()
    {
        Arrow arrow = new Arrow();
        arrow.idarrow = idarrow;
        arrow.x = x;
        arrow.y = y;
        arrow.z = z;
        arrow.xrot = xrot;
        arrow.yrot = yrot;
        arrow.zrot = zrot;
        arrow.destination = destination;
        arrow.room_idroom = room_idroom;
        return arrow;
    }
}