﻿[System.Serializable]
public class Arrow
{
    public int idarrow;
    public double x;
    public double y;
    public double z;
    public double xrot;
    public double yrot;
    public double zrot;
    public int destination;
    public int room_idroom;
}