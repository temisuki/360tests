﻿using System.Collections.Generic;

[System.Serializable]
public class Room
{
    public int idroom;
    public string name;
    public List<Arrow> arrows;
    public string image;
    public bool isFirst;
    public int version;
    public string thumbnail;

    public UnityEngine.Texture2D thumb { get; set; }
    //Saved texture
    public string PhotoName { get; set; }

    public string thumbnailName { get; set; }
}