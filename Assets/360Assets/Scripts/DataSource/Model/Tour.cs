﻿using System.Collections.Generic;

[System.Serializable]
public class Tour
{
    public List<Room> rooms;

    public Room findRoom()
    {
        foreach (Room room in rooms)
        {
            if (room.isFirst.Equals(true))
            {
                return room;
            }
        }
        return null;
    }

    public Room findRoom(int destination)
    {
        foreach (Room room in rooms)
        {
            if (room.idroom.Equals(destination))
            {
                return room;
            }
        }
        return null;
    }
}