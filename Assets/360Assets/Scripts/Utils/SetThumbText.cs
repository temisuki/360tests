﻿using UnityEngine;
using UnityEngine.UI;

public class SetThumbText : MonoBehaviour
{
    public void setTexture(Texture2D text)
    {
        this.gameObject.GetComponent<RawImage>().texture = text;
    }
}