﻿using UnityEngine;

public class MouseOverCheck : MonoBehaviour
{
    public GameObject mainScript;

    private void OnMouseOver()
    {
        mainScript.GetComponent<MainScript>().isOverPanel = true;
    }

    private void OnMouseExit()
    {
        mainScript.GetComponent<MainScript>().isOverPanel = false;
    }
}