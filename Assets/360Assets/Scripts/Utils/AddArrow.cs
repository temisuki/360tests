﻿using UnityEngine;

public class AddArrow : MonoBehaviour
{
    private int defScreenWidth;
    private int defScreenHeight;
    private bool isFullScreenNow;

    public void addArrow()
    {
        GameObject.FindGameObjectWithTag("Settings").GetComponent<RoomViewImpl>().createArrow();
    }
    private void Awake()
    {
        defScreenWidth = Screen.width;
        defScreenHeight = Screen.height;
    }

    public void ChangeScreenMode()
    {
        if (!IsFullScreen())
        {
            Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
        }
        else
        {
            Screen.SetResolution(defScreenWidth, defScreenHeight, false);
        }

        isFullScreenNow = !IsFullScreen();
    }

    public bool IsFullScreen()
    {
        return Application.platform == RuntimePlatform.WebGLPlayer ? Screen.fullScreen : isFullScreenNow;
    }
}