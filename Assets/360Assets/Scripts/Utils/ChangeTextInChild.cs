﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeTextInChild : MonoBehaviour
{
    public void changeText(string text)
    {
        this.gameObject.GetComponentInChildren<Text>().text = text;
    }
}