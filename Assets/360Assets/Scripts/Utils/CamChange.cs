﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamChange : MonoBehaviour
{
    public GameObject stereoscopicCamera;
    public GameObject normalCam;

    public void ChangeCam()
    {
        if (normalCam.active)
        {
            normalCam.SetActive(false);
            stereoscopicCamera.SetActive(true);
        }
        else
        {
            normalCam.SetActive(true);
            stereoscopicCamera.SetActive(false);
        }
    }
}
