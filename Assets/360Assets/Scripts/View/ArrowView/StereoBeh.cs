﻿using UnityEngine;
using UnityEngine.UI;

public class StereoBeh : MonoBehaviour
{
    public Camera stereoCam;
#if UNITY_IOS || UNITY_ANDROID
    private float amount;
    private float currentGazeTimeInSeconds = 0;
    private float holdGazeTimeInSeconds = 2;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
        if (stereoCam.isActiveAndEnabled)
        {
            Vector3 screenPoint = stereoCam.WorldToViewportPoint(this.gameObject.transform.position);
            if (screenPoint.z > 0.3 && screenPoint.x > 0.3 && screenPoint.x < 0.55 && screenPoint.y > 0.3 && screenPoint.y < 0.55)
            {
                currentGazeTimeInSeconds += Time.deltaTime;
                amount = ((currentGazeTimeInSeconds / (float)0.02)) / 100;
                this.gameObject.GetComponentInChildren<Image>().fillAmount = amount;
                if (currentGazeTimeInSeconds >= holdGazeTimeInSeconds)
                {
                    this.gameObject.GetComponentInChildren<Image>().fillAmount = 0;
                    currentGazeTimeInSeconds = 0;
                    this.gameObject.GetComponentInChildren<Button>().onClick.Invoke();
                }
            }
            else
            {
                this.gameObject.GetComponentInChildren<Image>().fillAmount = 0;
                currentGazeTimeInSeconds = 0;
            }
        }
    }
#endif
}