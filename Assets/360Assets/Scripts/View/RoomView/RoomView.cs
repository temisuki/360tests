﻿using UnityEngine;

public class RoomViewImpl : MonoBehaviour, Contract.RoomView
{
    private Contract.RoomPresenter presenter;
    private GameObject roomAsSphere;
    private GameObject arrowScheme;
    private GameObject createdArrows;
    private GameObject loading;

    public void Start()
    {
        arrowScheme = GameObject.FindGameObjectWithTag("ArrowScheme");
        roomAsSphere = GameObject.FindGameObjectWithTag("Room");
        createdArrows = GameObject.FindGameObjectWithTag("CreatedArrows");
#if UNITY_ANDROID || UNITY_IOS
        presenter = new UserRoomPresenter (this, gameObject.AddComponent<ApiDataSource>() as ApiDataSource, gameObject.AddComponent<PreferenceDataSource>() as PreferenceDataSource);
#endif
#if UNITY_WEBGL
        presenter = new EditorRoomPresenter(this, gameObject.AddComponent<WebGLDataSource>() as WebGLDataSource, gameObject.AddComponent<PreferenceDataSource>() as PreferenceDataSource);
#endif
        presenter.getTour();
    }

    public void showRoom(Room room, CallbackPattern callbackPattern)
    {
#if UNITY_WEBGL
        roomAsSphere.GetComponentInChildren<Renderer>().material.mainTexture = callbackPattern.texture;
#endif
#if UNITY_ANDROID || UNITY_IOS
        roomAsSphere.GetComponentInChildren<Renderer>().material.mainTexture = presenter.loadTexture(room);
#endif
        Debug.Log(roomAsSphere.GetComponentInChildren<Renderer>().material.mainTexture.width + " " + roomAsSphere.GetComponentInChildren<Renderer>().material.mainTexture.height);
        showArrows(room);
    }

    public void showTexture(CallbackPattern callbackPattern)
    {
        roomAsSphere.GetComponentInChildren<Renderer>().material.mainTexture = callbackPattern.texture;
    }

    public void showArrows(Room room)
    {
        //DestroyArrows();
        for (int x = 0; x < room.arrows.Count; x++)
        {
            GameObject newArrow = Instantiate(arrowScheme);
            presenter.addArrowToDestroyList(newArrow);
            newArrow.GetComponent<ArrowMono>().SetObject(room.arrows[x], presenter);
        }
    }

    private GameObject thumbScheme;
    private GameObject thumbScheme2;
    private GameObject Aim;
    private GameObject panel;

    public void showList(Room room, CallbackPattern callbackPattern)
    {
        if (thumbScheme == null) thumbScheme = GameObject.FindGameObjectWithTag("Thumbnail");
        if (thumbScheme2 == null) thumbScheme2 = GameObject.FindGameObjectWithTag("Thumb");
        if (panel == null) panel = GameObject.FindGameObjectWithTag("Panel");
        Thumbs(thumbScheme, null, room, callbackPattern);
        Thumbs(thumbScheme2, panel, room, callbackPattern);
    }

    public void Thumbs(GameObject thumb, GameObject panel, Room room, CallbackPattern callbackPattern)
    {
        if (panel != null) panel.SetActive(true);
        thumb.SetActive(true);
        GameObject newThumb = Instantiate(thumb);
        newThumb.gameObject.tag = "Untagged";
        thumb.SetActive(false);
        newThumb.GetComponent<ButtonModel>().SetList(thumb.transform.parent.gameObject, room, callbackPattern, presenter);
        if (panel != null) panel.SetActive(false);
    }

    public bool checkIfObjectExist()
    {
        if (GameObject.FindGameObjectWithTag("Thumb")) return true;
        else return false;
    }

    public void disableButtonScheme()
    {
    }
    public void setPanelActive(bool active)
    {
        panel.SetActive(active);
    }

    public void DestroyArrows(GameObject arrow)
    {
        Destroy(arrow);
    }

    public void destroyTextures(Texture texture)
    {
        Destroy(texture);
    }

    private GameObject aim;

    public void createArrow()
    {
        aim = GameObject.FindGameObjectWithTag("Aim");
        GameObject newArrow = Instantiate(GameObject.FindGameObjectWithTag("ArrowScheme"));
        ArrowMono arrowMono = newArrow.GetComponent<ArrowMono>();
        Arrow arrow = new Arrow();
        arrow.room_idroom = presenter.getActiveRoomId();
        newArrow.transform.position = aim.transform.position;
        newArrow.transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);
        newArrow.transform.Rotate(new Vector3(newArrow.transform.rotation.x, newArrow.transform.rotation.y - 180f, newArrow.transform.position.z));

        arrow.x = newArrow.transform.position.x;
        arrow.y = newArrow.transform.position.y;
        arrow.z = newArrow.transform.position.z;

        arrow.xrot = newArrow.transform.rotation.eulerAngles.x;
        arrow.yrot = newArrow.transform.rotation.eulerAngles.y;
        arrow.zrot = newArrow.transform.rotation.eulerAngles.z;

        arrowMono.SetObject(presenter, arrow);
        presenter.addArrowToDestroyList(newArrow);
        GameObject.FindGameObjectWithTag("Settings").GetComponent<MainScript>().arrow = arrowMono;
    }

    public void addArrow(Arrow arrow)
    {
        GameObject newArrow = Instantiate(arrowScheme);
        presenter.addArrowToDestroyList(newArrow);
        newArrow.GetComponent<ArrowMono>().SetObject(arrow, presenter);
    }
}