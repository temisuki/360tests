﻿using UnityEngine;
namespace Contract
{
    public interface RoomView
    {
        void showTexture(CallbackPattern callbac);

        void showRoom(Room room, CallbackPattern callback);

        void DestroyArrows(GameObject arrow);

        void destroyTextures(Texture texture);

        void showArrows(Room room);

        void showList(Room room, CallbackPattern callbackPattern);

        void disableButtonScheme();

        void createArrow();

        bool checkIfObjectExist();

        void setPanelActive(bool active);
    }

    public interface RoomPresenter
    {
        void getTour();

        void addArrow(ArrowMono arrow);

        void addArrowToDestroyList(GameObject arrow);

        void getRoom(int id);

        void getTexture();

        void getRoom();

        void getRoom(Room room);

        Texture2D loadTexture(Room room);

        string findRoom(int id);

        Texture2D loadThumbnail(Room room);

        bool checkIfExist(string filename);

        bool checkIfUpdate(Room room);

        void deleteArrow(ArrowMono arrowMono);

        void destroyArrows();



        void manageArrow(Arrow arrow, int id, bool update);

        void setPanelActive(bool active);

        int getActiveRoomId();
    }
}
