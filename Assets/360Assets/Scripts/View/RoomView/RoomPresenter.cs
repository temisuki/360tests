﻿using System;
using System.Collections.Generic;
using Contract;
using UnityEngine;
using System.IO;

public class RoomPresenterImpl : RoomPresenter
{
    private RoomView view;
    private IApiDataSource apiDataSource;
    private IPreferenceDataSource preferenceDataSource;
    private Tour tour;
    private Tour lastTour;
    private List<GameObject> arrowsToDestroy;
    private Room activeRoom;

    public RoomPresenterImpl(RoomView view, IApiDataSource apiDataSource, IPreferenceDataSource preferenceDataSource)
    {
        this.preferenceDataSource = preferenceDataSource;
        this.apiDataSource = apiDataSource;
        this.view = view;
    }

    public void getRoom(int id)
    {
        destroyArrows();
        activeRoom = tour.findRoom(id);
        activeRoom.PhotoName = activeRoom.image.Substring(36);
        if (!checkIfExist(activeRoom.PhotoName) || !checkIfUpdate(activeRoom)) apiDataSource.getTexture(activeRoom, callback =>
        {
            if (callback.isDownloadDone)
            {
                view.showRoom(activeRoom, callback);
                GC.Collect();
            }
        });
        else
        {
            view.showRoom(activeRoom, new CallbackPattern());
            GC.Collect();
        }
    }

    public void getTour()
    {
        lastTour = preferenceDataSource.getLastTour();
        arrowsToDestroy = new List<GameObject>();
        apiDataSource.getTour(preferenceDataSource.getTourId(), tour =>
        {
            this.tour = tour;
            getRoom();
        });
    }
    //public void getRoom()
    //{
    //    preferenceDataSource.saveLastTour(tour);

    //    if (tour.rooms.Count > 0)
    //    {
    //        for (int x = 0; x < tour.rooms.Count; x++)
    //        {
    //            if (tour.rooms[x].isFirst)
    //            {
    //                activeRoom = tour.rooms[x];
    //                destroyArrows();
    //                activeRoom.PhotoName = activeRoom.image.Substring(36);
    //                if (!checkIfExist(activeRoom.PhotoName) || !checkIfUpdate(activeRoom))
    //                {
    //                    apiDataSource.GetTexture2(activeRoom, this);
    //                }
    //                else
    //                {
    //                    view.showRoom(activeRoom);
    //                }
    //            }
    //        }
    //    }
    //}

    public void getRoom()
    {
        preferenceDataSource.saveLastTour(tour);
        if (tour.rooms.Count > 0)
        {
            for (int x = 0; x < tour.rooms.Count; x++)
            {
                if (tour.rooms[x].isFirst)
                {
                    activeRoom = tour.rooms[x];
                    destroyArrows();
                    activeRoom.PhotoName = activeRoom.image.Substring(36);
                    if (!checkIfExist(activeRoom.PhotoName) || !checkIfUpdate(activeRoom))
                    {
                        apiDataSource.getTexture(activeRoom, callback =>
                        {
                            if (callback.isDownloadDone)
                            {
                                view.showRoom(activeRoom, callback);
                                System.GC.Collect();
                                Resources.UnloadUnusedAssets();
                            }
                        });
                    }
                    else
                    {
                        view.showRoom(activeRoom, new CallbackPattern());
                        System.GC.Collect();
                        Resources.UnloadUnusedAssets();
                    }
                }
            }
        }
    }

    public void destroyArrows()
    {
        if (arrowsToDestroy.Count > 0)
        {
            foreach (GameObject arrow in arrowsToDestroy)
            {
                view.DestroyArrows(arrow);
            }
            arrowsToDestroy.Clear();
        }
    }

    public void addArrowToDestroyList(GameObject arrow)
    {
        arrowsToDestroy.Add(arrow);
    }

    public void deleteArrow(ArrowMono arrowMono)
    {
        for (int y = 0; y < tour.rooms.Count; y++)
        {
            if (tour.rooms[y].idroom.Equals(activeRoom.idroom))
            {
                for (int x = 0; x < activeRoom.arrows.Count; x++)
                {
                    if (activeRoom.arrows[x].idarrow.Equals(arrowMono.idarrow))
                    {
                        apiDataSource.deleteArrow(arrowMono.idarrow);
                        tour.rooms[y].arrows.RemoveAt(x);
                    }
                }
            }
        }
    }

    public string findRoom(int id)
    {
        return tour.findRoom(id).name;
    }

    public void OnApplicationFocus(bool focus)
    {
        //        Debug.Log("OnAppFocus");
        //#if !UNITY_WEBGL
        //        if(focus == true)
        //        {
        //            apiDataSource.getTour(tour =>
        //            {
        //                foreach(Room room in this.tour.rooms)
        //                {
        //                    view.destroyTextures(room.texture);
        //                }
        //                this.tour = tour;
        //                getTextures();
        //            });
        //        }

        //#endif
    }

    public Texture2D loadTexture(Room room)
    {
        return apiDataSource.LoadTexture(room.PhotoName);
    }
    public bool checkIfExist(string filename)
    {
        bool temp = File.Exists(Application.persistentDataPath + filename);
        return temp;
    }

    public bool checkIfUpdate(Room room)
    {
        if (lastTour != null)
        {
            foreach (Room lastRoom in lastTour.rooms)
            {
                if (lastRoom.idroom.Equals(room.idroom) && lastRoom.version.Equals(room.version))
                {
                    return true;
                }
                if (lastRoom.idroom.Equals(room.idroom) && !lastRoom.version.Equals(room.version))
                {
                    lastTour.rooms.RemoveAt(lastTour.rooms.IndexOf(lastRoom));
                    lastTour.rooms.Add(room);
                    return false;
                }
            }
        }
        return false;
    }

    public Arrow transformMonoToNormal(ArrowMono arrowMono)
    {
        throw new NotImplementedException();
    }

    public void manageArrow(Arrow arrow, int id, bool update)
    {
        throw new NotImplementedException();
    }
    public void createArrow()
    {
        throw new NotImplementedException();
    }

    public int getActiveRoomId()
    {
        throw new NotImplementedException();
    }

    public void getRoom(Room room)
    {
        throw new NotImplementedException();
    }


    public void setPanelActive()
    {
        throw new NotImplementedException();
    }

    public void setPanelActive(bool active)
    {
        throw new NotImplementedException();
    }

    public void addArrow(ArrowMono arrow)
    {
        throw new NotImplementedException();
    }

    public void setList()
    {
        throw new NotImplementedException();
    }

    public Texture2D loadThumbnail(Room room)
    {
        return apiDataSource.LoadTexture(room.thumbnailName);
    }

    public void getTexture()
    {
        throw new NotImplementedException();
    }
}