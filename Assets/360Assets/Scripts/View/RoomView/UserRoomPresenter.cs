﻿using Contract;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UserRoomPresenter : RoomPresenter
{
    private RoomView view;
    private IApiDataSource apiDataSource;
    private IPreferenceDataSource preferenceDataSource;
    private Tour tour;
    private Tour lastTour;
    private List<GameObject> arrowsToDestroy;
    private Room activeRoom;

    public UserRoomPresenter(RoomView view, IApiDataSource apiDataSource, IPreferenceDataSource preferenceDataSource)
    {
        this.preferenceDataSource = preferenceDataSource;
        this.apiDataSource = apiDataSource;
        this.view = view;
    }

    public void getTour()
    {
        lastTour = preferenceDataSource.getLastTour();
        Debug.Log(lastTour);
        arrowsToDestroy = new List<GameObject>();
        apiDataSource.getTour(preferenceDataSource.getTourId(), tour =>
        {
            this.tour = tour;
            getRoom();
        });
    }

    public void getRoom(int id)
    {
        destroyArrows();
        activeRoom = tour.findRoom(id);
        activeRoom.PhotoName = activeRoom.image.Substring(36);
        if (!checkIfExist(activeRoom.PhotoName) || !checkIfUpdate(activeRoom))
        {
            Debug.Log("Need to download");
            apiDataSource.getTexture(activeRoom, callback =>
            {
                if (callback.isDownloadDone)
                {
                    view.showRoom(activeRoom, callback);
                    GC.Collect();
                }
            });
        }
        else
        {
            Debug.Log("Existing");
            view.showRoom(activeRoom, new CallbackPattern());
            GC.Collect();
        }
    }

    public void getRoom()
    {
        preferenceDataSource.saveLastTour(tour);
        if (tour.rooms.Count > 0)
        {
            for (int x = 0; x < tour.rooms.Count; x++)
            {
                if (tour.rooms[x].isFirst)
                {
                    activeRoom = tour.rooms[x];
                    destroyArrows();
                    activeRoom.PhotoName = activeRoom.image.Substring(36);
                    if (!checkIfExist(activeRoom.PhotoName) || !checkIfUpdate(activeRoom))
                    {
                        Debug.Log("Need to download");
                        apiDataSource.getTexture(activeRoom, callback =>
                        {
                            if (callback.isDownloadDone)
                            {
                                view.showRoom(activeRoom, callback);
                                System.GC.Collect();
                                Resources.UnloadUnusedAssets();
                            }
                        });
                    }
                    else
                    {
                        Debug.Log("Loading existing");
                        view.showRoom(activeRoom, new CallbackPattern());
                        System.GC.Collect();
                        Resources.UnloadUnusedAssets();
                    }
                }
            }
        }
    }

    public void getRoom(Room room)
    {
        throw new NotImplementedException();
    }

    public void addArrowToDestroyList(GameObject arrow)
    {
        arrowsToDestroy.Add(arrow);
    }

    public bool checkIfExist(string filename)
    {
        return File.Exists(Path.Combine(Application.persistentDataPath, filename));
    }

    public bool checkIfUpdate(Room room)
    {
        if (lastTour != null)
        {
            Debug.Log("OldOneSaved");
            foreach (Room lastRoom in lastTour.rooms)
            {
                if (lastRoom.idroom.Equals(room.idroom) && lastRoom.version.Equals(room.version))
                {
                    return true;
                }
                if (lastRoom.idroom.Equals(room.idroom) && !lastRoom.version.Equals(room.version))
                {
                    Debug.Log("Update");
                    lastTour.rooms.RemoveAt(lastTour.rooms.IndexOf(lastRoom));
                    lastTour.rooms.Add(room);
                    return false;
                }
            }
        }
        return true;
    }

    public void destroyArrows()
    {
        foreach (GameObject arrow in arrowsToDestroy)
        {
            view.DestroyArrows(arrow);
        }
        arrowsToDestroy.Clear();
    }

    public string findRoom(int id)
    {
        return tour.findRoom(id).name;
    }

    public int getActiveRoomId()
    {
        return activeRoom.idroom;
    }

    public void getTexture()
    {
        throw new NotImplementedException();
    }

    public Texture2D loadTexture(Room room)
    {
        return apiDataSource.LoadTexture(room.PhotoName);
    }

    public Texture2D loadThumbnail(Room room)
    {
        throw new NotImplementedException();
    }

    public void manageArrow(Arrow arrow, int id, bool update)
    {
        throw new NotImplementedException();
    }

    public void setPanelActive(bool active)
    {
        throw new NotImplementedException();
    }

    public void deleteArrow(ArrowMono arrowMono)
    {
        throw new NotImplementedException();
    }

    public void addArrow(ArrowMono arrow)
    {
        throw new NotImplementedException();
    }
}