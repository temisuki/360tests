﻿using Contract;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EditorRoomPresenter : RoomPresenter
{
    private RoomView view;
    private IApiDataSource apiDataSource;
    private IPreferenceDataSource preferenceDataSource;
    private Tour lastTour;
    private List<GameObject> arrowsToDestroy;
    private Room activeRoom;
    public Tour tour;

    public EditorRoomPresenter(RoomView view, IApiDataSource apiDataSource, IPreferenceDataSource preferenceDataSource)
    {
        this.preferenceDataSource = preferenceDataSource;
        this.apiDataSource = apiDataSource;
        this.view = view;
    }

    public void getTour()
    {
        lastTour = preferenceDataSource.getLastTour();
        arrowsToDestroy = new List<GameObject>();
        apiDataSource.getTour(preferenceDataSource.getTourId(), tour =>
        {
            this.tour = tour;
            if (view.checkIfObjectExist())
            {
                setList();
                view.disableButtonScheme();
            }
            getRoom();
        });
    }

    public void addArrow(ArrowMono arrowMono)
    {
        Debug.Log(arrowMono.idarrow);
        bool isUpdate = false;
        for (int x = 0; x < activeRoom.arrows.Count; x++)
        {
            if (activeRoom.arrows[x].idarrow.Equals(arrowMono.idarrow))
            {
                manageArrow(arrowMono.GetArrowFromMono(), x, true);
                isUpdate = true;
            }
        }
        if (!isUpdate)
            manageArrow(arrowMono.GetArrowFromMono(), 0, false);
    }

    //
    public void manageArrow(Arrow arrow, int id, bool update)
    {
        if (update)
        {
            activeRoom.arrows[id] = arrow;
            apiDataSource.putArrow(arrow);
        }
        else
        {
            apiDataSource.postArrow(arrow, callback =>
            {
                arrow.idarrow = callback.id;
                activeRoom.arrows.Add(arrow);
                destroyArrows();
                view.showArrows(activeRoom);
                Debug.Log(arrow.idarrow);
            });
        }
    }

    public void deleteArrow(ArrowMono arrowMono)
    {
        for (int x = 0; x < activeRoom.arrows.Count; x++)
        {
            if (activeRoom.arrows[x].idarrow.Equals(arrowMono.idarrow))
            {
                apiDataSource.deleteArrow(arrowMono.idarrow);
                activeRoom.arrows.RemoveAt(x);
            }
        }
    }

    public void destroyArrows()
    {
        foreach (GameObject arrow in arrowsToDestroy)
        {
            view.DestroyArrows(arrow);
        }
        arrowsToDestroy.Clear();
    }

    private GameObject splash;

    public void getTexture()
    {
        Debug.Log("GetTexture");

        if (activeRoom.thumb != null)
        {
            CallbackPattern callbackPattern = new CallbackPattern();
            callbackPattern.texture = activeRoom.thumb;
            callbackPattern.isDownloadDone = true;
            view.showRoom(activeRoom, callbackPattern);
        }

        apiDataSource.getTexture(activeRoom, callback =>
        {
            if (callback.isDownloadDone)
            {
                //if (splash == null) splash = GameObject.FindGameObjectWithTag("Loading");
                //splash.SetActive(false);
                view.showRoom(activeRoom, callback);
                System.GC.Collect();
                Resources.UnloadUnusedAssets();
            }
        });
    }

    private void setList()
    {
        foreach (Room room in tour.rooms)
        {
            apiDataSource.getThumbnail(room, call =>
            {
                view.showList(room, call);
                room.thumb = call.texture;
            });
        }
    }

    public void setPanelActive(bool active)
    {
        view.setPanelActive(active);
    }

    public int getActiveRoomId()
    {
        return activeRoom.idroom;
    }

    public string findRoom(int id)
    {
        return tour.findRoom(id).name;
    }

    public void addArrowToDestroyList(GameObject arrow)
    {
        arrowsToDestroy.Add(arrow);
    }

    public void getRoom(int id)
    {
        destroyArrows();
        activeRoom = tour.findRoom(id);
        getTexture();
    }

    public void getRoom()
    {
        activeRoom = tour.findRoom();
        getTexture();
    }

    public void getRoom(Room room)
    {
        activeRoom = room;
        getTexture();
    }

    public Texture2D loadTexture(Room room)
    {
        throw new NotImplementedException();
    }

    public Texture2D loadThumbnail(Room room)
    {
        throw new NotImplementedException();
    }

    public bool checkIfExist(string filename)
    {
        throw new NotImplementedException();
    }

    public bool checkIfUpdate(Room room)
    {
        throw new NotImplementedException();
    }
}