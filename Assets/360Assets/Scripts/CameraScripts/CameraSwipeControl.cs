﻿using UnityEngine;

public class CameraSwipeControl : MonoBehaviour
{
#if UNITY_ANDROID || UNITY_IOS
    public Camera cam;
    private Vector3 firstpoint;
    private Vector3 secondpoint;
    private float xAngle = 0.0f; //angle for axes x for rotation
    private float yAngle = 0.0f;
    private float xAngTemp = 0.0f; //temp variable for angle
    private float yAngTemp = 0.0f;
    private GameObject playRot;

    void Start()
    {
        firstpoint = new Vector3(0, 0, 0);
        secondpoint = new Vector3(0, 0, 0);

        playRot = GameObject.Find("Camera");

        xAngle = 0.0f;
        yAngle = 0.0f;

        //transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);
    }

    void Update()
    {
        Input.gyro.enabled = true;

        if (Input.gyro.enabled)
        {
            if (Input.touchCount > 0)
            {
                this.GetComponent<Moving>().disableRotTri();
                //Touch began, save position
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    firstpoint = Input.GetTouch(0).position;
                    xAngTemp = xAngle;
                    yAngTemp = yAngle;
                }
                //Move finger by screen
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    secondpoint = Input.GetTouch(0).position;
                    //Mainly, about rotate camera. For example, for Screen.width rotate on 180 degree

                    if (yAngle < 0)
                        yAngle += 360;
                    if (yAngle > 360)
                        yAngle -= 360;

                    if (yAngle > 90 && yAngle < 270)
                        xAngle = xAngTemp - ((secondpoint.x - firstpoint.x) * 180.0f / Screen.width)*-1;
                    else
                        xAngle = xAngTemp + ((secondpoint.x - firstpoint.x) * 180.0f / Screen.width)*-1;

                    if (xAngle < 0)
                        xAngle += 360;

                    if (xAngle > 360)
                        xAngle -= 360;
                    this.GetComponent<Moving>().disableRotTri();
                    this.GetComponent<Moving>().ResetPos();

                    transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, xAngle, 0.0f);
                }
            }
        }
    }
#endif
}