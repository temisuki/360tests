﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepObjectInFront : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    void Update()
    {
        MoveSphere();
    }

    private void MoveSphere()
    {
        this.gameObject.transform.position = this.gameObject.transform.parent.gameObject.transform.position;
        this.gameObject.transform.rotation = this.gameObject.transform.parent.gameObject.transform.rotation;
        this.gameObject.transform.Translate(0.0f, 0.0f, 0.45f); // move away from the camera 
        this.gameObject.transform.LookAt(this.gameObject.transform.parent.gameObject.transform.position);
    }
}
