﻿using UnityEngine;

public class CameraMouse : MonoBehaviour
{
    public GameObject settings;
    public bool rightclicked;
    public float horizontalSpeed = 2.0F;
    public float verticalSpeed = 2.0F;

    private void Update()
    {
#if UNITY_EDITOR || UNITY_WEBGL
        if (settings == null) settings = GameObject.FindGameObjectWithTag("Settings");
        rightclicked = Input.GetMouseButton(0);
        if (transform.rotation.eulerAngles.z != 0) transform.Rotate(0, 0, -transform.rotation.eulerAngles.z);

        if (rightclicked && !settings.GetComponent<MainScript>().isOverPanel)
        {
            float h = horizontalSpeed * -Input.GetAxis("Mouse X");
            float v = verticalSpeed * Input.GetAxis("Mouse Y");
            transform.Rotate(v, h, 0f);
        }
#endif
    }
}