﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warning : MonoBehaviour {
    public GameObject arrow;
    public void delete()
    {
        arrow.GetComponent<ArrowMono>().Delete();
        arrow.SetActive(false);
    }
    public void setObject(GameObject arrow)
    {
        this.arrow = arrow;
    }
}
